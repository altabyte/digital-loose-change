class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :user
      t.datetime :time,           null: false
      t.integer :amount,          null: false, default: 0
      t.string :transaction_type, null: false
      t.string :name,             null: false
      t.timestamps null: false
    end
  end
end
