require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Creating a basic user' do
    subject(:user) { FactoryGirl.create(:user) }

    it { is_expected.to be_valid }

    it 'should have a unique ID' do
      expect(user.id).not_to be_nil
      puts "User ID:      #{user.id}"
    end

    it 'should have a username' do
      expect(user.username).not_to be_nil
      puts "Username:     #{user.username}"
    end

    it 'should have an email address' do
      expect(user.email).not_to be_nil
      puts "Email:        #{user.email}"
    end

    it 'should have an authentication token' do
      expect(user.authentication_token).not_to be_nil
      puts "Auth Token:   #{user.authentication_token}"
    end

  end


  describe 'Setting username to nil' do
    subject(:user) { FactoryGirl.create(:user, username: nil) }

    it 'should set the username to the user ID' do
      puts "User ID:      #{user.id}"
      puts "Username:     #{user.username}"
      #expect(user.username).to eq(user.id.to_s)
    end
  end

end
