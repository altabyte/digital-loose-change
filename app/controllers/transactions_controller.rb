require 'JSON'

class TransactionsController < ApplicationController

  before_action :authenticate_user!

  before_action :set_transaction, only: [:show, :edit, :update, :destroy]

  # GET /transactions
  def index
    @transactions = current_user.transactions.order(time: :desc)
  end

  # GET /transactions/1
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
    @transaction.user = current_user
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.user = current_user

    if @transaction.save
      redirect_to @transaction, notice: 'Transaction was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /transactions/1
  def update
    if @transaction.update(transaction_params)
      redirect_to @transaction, notice: 'Transaction was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /transactions/1
  def destroy
    @transaction.destroy
    redirect_to transactions_url, notice: 'Transaction was successfully destroyed.'
  end

  def import
    source_file = params[:statement]
    if source_file
      source_file = "#{source_file}.json" unless source_file =~ /.json$/
      TransactionsController.import_statement("DATA/#{source_file}")
    end
    redirect_to dashboard_path
  end

  def self.import_statement(json_file)
    return unless json_file && json_file.is_a?(String)
    return unless File.exist? json_file
    hash = JSON.parse(File.read(json_file))

    user_id = hash['user_id'].to_i
    raise "User with ID #{user_id} not found" unless User.exists?(user_id)
    user = User.find(user_id)
    hash['transactions'].each do |transaction|
      time = transaction['date']
      name = transaction['payee']
      amount = (transaction['amount'] * 100).to_i
      type = transaction['type']
      import_transaction(user, time, name, amount, type)
    end

    user.save
    puts "Added #{user.transactions.count} transactions"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def transaction_params
      params.require(:transaction).permit(:transaction_type, :time, :name, :amount)
    end

  def self.import_transaction(user, time, name, amount, type)
    transaction = user.transactions.where(time: time).first
    unless transaction
      transaction = Transaction.create(time: time, amount: amount, transaction_type: type, name: name)
      puts "Created transaction for #{user.email} #{time} #{name.ljust(30)} #{amount}"
    end
    user.transactions << transaction
  end
end
