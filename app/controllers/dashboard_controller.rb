class DashboardController < ApplicationController

  before_action :authenticate_user!

  def index
    @date_1 = Time.now
    begin
      @date_1 = Time.parse(params[:date_1])
    rescue
      puts 'could not parse date_1'
    end
    @date_2 = Time.now - 60.days
    begin
      @date_2 = Time.parse(params[:date_2])
    rescue
      puts 'could not parse date_2'
    end
    date_range = [@date_1, @date_2].sort
    @date_1 = date_range.first
    @date_2 = date_range.last
    @days = (@date_2 - @date_1).to_i / (24 * 60 * 60)

    @transactions = current_user.transactions.where('time > ?', @date_1).where('time < ?', @date_2).order(time: :desc)
    @savings = 0
    @chart_data = []
    @transactions.each do |transaction|
      @savings += transaction.saving_amount
      @chart_data << @savings / 100.0 # if @savings > 0.00
    end
  end

  def community

  end

  def donations

  end

  def settings
    @user = current_user
  end
end
