class Transaction < ActiveRecord::Base

  belongs_to :user


  validates :time,              presence: true
  validates :amount,            presence: true
  validates :transaction_type,  presence: true
  validates :name,              presence: true

  def credit?
    self.amount > 0
  end

  def debit?
    !credit?
  end

  def debit_card_transaction?
    self.transaction_type.downcase == 'debit card transaction'
  end

  def saving_amount
    return 0 unless debit_card_transaction?
    pennies = 100 - self.amount.to_s[-2, 2].to_i
    pennies
  end
end
