class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable

  has_many :transactions

  before_validation(on: :create) { ensure_authentication_token }
  before_validation { ensure_user_id }

  validates :username, uniqueness: true

  #---------------------------------------------------------------------------
  private

  def ensure_user_id
    self.username ||= self.id.to_s
  end

  def ensure_authentication_token
    self.authentication_token ||= generate_authentication_token
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end
end
